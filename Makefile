default: carnet vrac package-xparam test todo languages

vrac: vrac.pdf
vrac.pdf: vrac.tex
	latex '\nonstopmode \input vrac'
	dvips vrac
	ps2pdf vrac.ps vrac.pdf
	rm -f *.dvi *.ps

languages: BabelLanguages.pdf LocalesLanguages.pdf
BabelLanguages.pdf: BabelLanguages.tex
	pdflatex '\nonstopmode \input BabelLanguages'
LocalesLanguages.pdf: LocalesLanguages.tex
	pdflatex '\nonstopmode \input LocalesLanguages'

carnet: carnet.pdf
carnet.pdf: carnet.tex
	latex '\nonstopmode \input carnet'
	dvips carnet
	ps2pdf carnet.ps carnet.pdf
	rm -f *.dvi *.ps

package-xparam: xparam.tex xparam.pdf
xparam.tex: xparam.dtx xparam.ins
	pdftex xparam.ins
xparam.pdf: xparam.dtx xparam.ins
	pdflatex '\nonstopmode \input xparam.dtx'
	makeindex -s gind.ist -o xparam.ind xparam.idx
	makeindex -s gglo.ist -o xparam.gls xparam.glo

test: test-tex.pdf test-xetex.pdf test-latex.pdf test-xelatex.pdf
test-tex.pdf: test-tex.tex xparam.tex
	pdftex '\nonstopmode \input test-tex'
test-xetex.pdf: test-xetex.tex xparam.tex
	xetex  '\nonstopmode \input test-xetex'
test-latex.pdf:  test-latex.tex xparam.tex
	pdflatex '\nonstopmode \input test-latex'
test-xelatex.pdf:  test-xelatex.tex xparam.tex
	xelatex  '\nonstopmode \input test-xelatex'

todo: todo.pdf
todo.pdf: todo.tex
	pdflatex '\nonstopmode \input todo'

clean:
	rm -f *~ *.aux *.log *.dvi *.glo *.gls *.idx *.ind *.toc *.tmp
