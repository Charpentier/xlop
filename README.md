xlop est un package LaTeX qui permet d'écrire des opérations en ligne comme
  \[12 + 39 = 51\]
ou en les posant comme
\begin{verbatim}
  12
 +
  39
 ---
  51
\end{verbatim}
voire même avec des retenues comme
\begin{verbatim}
  1
  12
 +
  39
 ___
  51
\end{verbatim}

xlop permet de gérer de façon automatique toute ces opérations. Par exemple, les exemples précédents s'obtiennent avec des variantes de la commande :
\begin{verbatim}
\opadd{12}{39}
\end{verbatim}

xlop gère  l'écriture des opérations selon un très grand nombre de règles dépendant des couples pays/langue\ldots{} oui tous les êtres humains n'écrivent pas les opérations de la même façon !
